class Calc {

    controleVar(a, b) {
        if(typeof a != "number" || typeof b != "number") {
            return false
        } else {
            return true
        }
    }

    controleLength(a, b) {
        const lengthA = a.toString().length
        const lengthB = b.toString().length
        if (lengthA < lengthB) {
            return Math.pow(10, lengthB)
        } else {
            return Math.pow(10, lengthA)
        }
    }

    add(a, b) {
        if (this.controleVar(a, b)) {
            const data = this.controleLength(a, b)
            return (a*data + b*data)/data
            
        } else {
            return "error not a number"
        }
        
    }

    sub(a, b) {
        if (this.controleVar(a, b)) {
            const data = this.controleLength(a, b)
            return (a*data - b*data)/data
        } else {
            return "error not a number"
        } 
    }

    mul(a, b) {
        if (this.controleVar(a, b)) {
            const data = this.controleLength(a, b)
            return (a*data * b*data)/Math.pow(data, 2)
        } else {
            return "error not a number"
        } 
    }

    div(a, b) {
        if (this.controleVar(a, b)) {
            if ( b == 0 ) {
                return "error infinity"
            } else {
                const data = this.controleLength(a, b)
                return (a*data) / (b*data)
            }

        } else {
            return "error not a number"
        }
    }
}

exports.Calc = Calc