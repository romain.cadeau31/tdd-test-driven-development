const fizzbuzz = (num) => {
    switch (true) {
        case num % 5 == 0 && num % 7 ==0:
            return "fizzbuzz"
        case num % 5 == 0:
            return "buzz"
        case num%7==0:
            return "fizz"
        case !num:
            return 'Error!'
        default :
            return ''
    }
    // return (num % 5 == 0 ? "buzz" : (num % 7 == 0?"fizz":""));
}

exports.fizzbuzz = fizzbuzz;