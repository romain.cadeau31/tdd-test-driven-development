const expect = require('chai').expect;
var assert = require('chai').assert
var calc = require('../src/calc');

describe("number", () => {
    const Calc = calc.Calc;
    var myCalc;
    beforeEach(function () {
        myCalc = new Calc();
        console.log(myCalc)
    });

    
    describe("#add", () => {

        it("should add numbers if it's number", () => {
            expect(myCalc.add(0.0003, 0.0000000006)).to.equal(0.0003000006)
        })
        it("should return error if it's not a number string", () => {
            expect(myCalc.add("6", 3)).to.equal("error not a number")
        })
        it("should return error if it's not a number array", () => {
            expect(myCalc.add(2, [0, 1])).to.equal("error not a number")
        })
    })

    describe("#sub", () => {

        it("should sub numbers if it's number", () => {
            expect(myCalc.sub(3, 2)).to.equal(1)
        })
        it("should sub numbers if it's number", () => {
            expect(myCalc.sub(2, 3)).to.equal(-1)
        })
        it("should return error if it's not a number string", () => {
            expect(myCalc.sub("6", 3)).to.equal("error not a number")
        })

    })

    describe("#mul", () => {
        it("should mul numbers if it's number", () => {
            expect(myCalc.mul(0.6, 3)).to.equal(1.8)
        })
        it("should mul numbers if it's number", () => {
            expect(myCalc.mul(2, -5)).to.equal(-10)
        })
        it("should return error if it's not a number", () => {
            expect(myCalc.mul("6", 3)).to.equal("error not a number")
        })
        it("should mul numbers if it's number", () => {
            expect(myCalc.mul(0.03, 0.02)).to.equal(0.0006)
        })

    })

    describe("#div", () => {
        it("should div numbers if it's number", () => {
            expect(myCalc.div(4, 2)).to.equal(2)
        })

        it("should div numbers if it's number", () => {
            expect(myCalc.div(0.3, 2)).to.equal(0.15)
        })

        it("should not div numbers if one is not a number", () => {
            expect(myCalc.div(4, "test")).to.equal("error not a number")
        })

        it("should not div numbers if the seconde number is 0", () => {
            expect(myCalc.div(4, 0)).to.equal("error infinity")
        })

    })
})